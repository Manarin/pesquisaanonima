<?php

use Phinx\Migration\AbstractMigration;

class Pesquisa extends AbstractMigration
{

    public function up()
    {
     $tab = $this->table("pesquisa");
     $tab->addColumn("titulo", "string", ["limit" => 300]);
     $tab->addColumn("descricao", "text");
     $tab->addColumn("pergunta", "string", ["limit" => 300]);
     $tab->addColumn("resposta", "string", ["limit" => 300]);
    }
    public function down()
    {
        $this->dropTable('pesquisa');
    }
}
