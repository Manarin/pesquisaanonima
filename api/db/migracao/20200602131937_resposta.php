<?php

use Phinx\Migration\AbstractMigration;

class Resposta extends AbstractMigration
{

    public function up()
    {
        $tab = $this->table("resposta");
        $tab->addColumn("resposta", "boolean", ["limit" => 2]);
    }
    public function down()
    {
        $this->dropTable("resposta");
    }
}
