<?php

use Phinx\Migration\AbstractMigration;

class Usuario extends AbstractMigration
{
    public function up()
    {
        $tab = $this->table("usuario");
        $tab->addColumn("usuario", "string", ["limit" => 10]);
        $tab->addColumn("senha", "text");
        $tab->addColumn("email", "text");
        $tab->addColumn("token", "text");
    }
    public function down()
    {
        $this->dropTable("usuario");
    }
}
