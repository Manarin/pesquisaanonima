CREATE DATABASE bancopesquisa;
USE bancopesquisa;

CREATE TABLE pesquisa(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(300) NOT NULL,
    descricao TEXT NOT NULL,
    pergunta VARCHAR(300) NOT NULL,
    resposta VARCHAR(10) NOT NULL
);

CREATE TABLE resposta(
    id_resposta INT PRIMARY KEY AUTO_INCREMENT,
    resposta VARCHAR(10) NOT NULL,
    id_pesquisa INT NOT NULL,
    constraint fk_pesquisa FOREIGN KEY (id_pesquisa)
    REFERENCES pesquisa (id)
 );

 CREATE TABLE usuario(
     id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
     usuario VARCHAR(10) NOT NULL,
     senha VARCHAR(20) NOT NULL,
     email TEXT NOT NULL,
     token TEXT NOT NULL
 );

