<?php

namespace Source\Controllers;

use Source\Models\Usuario;
use Source\Models\Validacao;

class Usuarios {

    static function CriarUsuario() {

        $dados = json_decode(file_get_contents("php://input"),false);

        if(!$dados){
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"Nenhum dado inserido!"));
            exit;
        }

        $erros = array();

        if(!Validacao::validacaoString($dados->usuario)) {
            array_push($erros,"Usuário");
        }
        if(!Validacao::validacaoString($dados->senha)) {
            array_push($erros,"Senha");
        }
        if(!Validacao::validacaoEmail($dados->email)) {
            array_push($erros,"Email");
        }

        if(count($erros) > 0) {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"Há campos invalidos no formulario!", "fields"=>$erros));
            exit;
        }

        //$filtroToken = Usuario::where('usuario', '=', $dados->usuario)->first('token');
        $filtroUsuario = Usuario::where('usuario', '=', $dados->usuario)->first('usuario');
        $filtroEmail = Usuario::where('email', '=', $dados->email)->first('email');

        if($filtroUsuario || $filtroEmail) {

            if($filtroUsuario) {
                echo json_encode(array("resposta"=>"Usuário existente, por favor, trocar nome do usuário!"));
            }
            if($filtroEmail) {
                echo json_encode(array("resposta"=>"Email existente, por favor, trocar a conta de email!"));
            }

            exit;

        }

        $usuarios = new Usuario();
        $usuarios->usuario = $dados->usuario;
        $usuarios->email = $dados->email;
        $usuarios->senha = $dados->senha;
        $usuarios->token = CriarToken::token($usuarios);
        $usuarios->save();

        header("HTTP/1.1 201 Created");
        echo json_encode(array("response"=> "Usuário criado com sucesso!"));

    }

    static function BuscarUsuario() {

        $usuarios = Usuario::all();

        if($usuarios) {

            $buscar = array();
            foreach($usuarios as $usuario) {

                array_push($buscar, $usuario);

            }

            echo json_encode(array("response" => $buscar));

        } else {

            echo json_encode(array("response" => "Nenhuma Pesquisa foi localizada!"));

        }

    }

}