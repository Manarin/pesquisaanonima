<?php

namespace Source\Controllers;

use Source\Models\Pesquisa;
use Source\Models\Validacao;

class Pesquisas {

    static function CriarPesquisa() {

                $dados = json_decode(file_get_contents("php://input"),false);
                
                if(!$dados){
                    header("HTTP/1.1 400 Bad Request");
                    echo json_encode(array("response"=>"Nenhum dado inserido!"));
                    exit;
                }
        
                $erros = array();
        
                if(!Validacao::validacaoString($dados->titulo)) {
                    array_push($erros,"Titulo");
                }
                if(!Validacao::validacaoString($dados->descricao)) {
                    array_push($erros,"Descrição");
                }
                if(!Validacao::validacaoString($dados->pergunta)) {
                    array_push($erros,"Pergunta");
                }
                if(!Validacao::validacaoString($dados->resposta)) {
                    array_push($erros,"Resposta");
                }
        
                if(count($erros) > 0) {
                    header("HTTP/1.1 400 Bad Request");
                    echo json_encode(array("response"=>"Há campos invalidos no formulario!", "fields"=>$erros)); 
                    exit;
                }
        
                $pesquisas = new Pesquisa();
                $pesquisas->titulo = $dados->titulo;
                $pesquisas->descricao = $dados->descricao;
                $pesquisas->pergunta = $dados->pergunta;
                $pesquisas->resposta = $dados->resposta;
                $pesquisas->save();
                
                header("HTTP/1.1 201 Created");
                echo json_encode(array("response"=> "Pesquisa criada com sucesso!"));

    }

    static function BuscarPesquisa() {

        $pesquisas = Pesquisa::all();

        if($pesquisas) {

            $buscar = array();
            foreach($pesquisas as $pesquisa) {

                array_push($buscar, $pesquisa);

            }

            echo json_encode(array("response" => $buscar));

        } else {

            echo json_encode(array("response" => "Nenhuma Pesquisa foi localizada!"));

        }

    }

    static function AtualizarPesquisa() {

        $dados = json_decode(file_get_contents('php://input'), false);

        if(!$dados){
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response" => "Nenhum dado inserido!"));
            exit;
        }

        $erros = array();

        if(!Validacao::validacaoString($dados->titulo)) {
            array_push($erros,"Titulo");
        }
        if(!Validacao::validacaoString($dados->descricao)) {
            array_push($erros,"Descrição");
        }
        if(!Validacao::validacaoString($dados->pergunta)) {
            array_push($erros,"Pergunta");
        }
        if(!Validacao::validacaoString($dados->resposta)) {
            array_push($erros,"Resposta");
        }

        if(count($erros) > 0) {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"Há campos invalidos no formulario!", "fields"=>$erros));
            exit;
        }

        $pesquisaId = filter_input(INPUT_GET, "id_pesquisa");
        $pesquisas = Pesquisa::find($pesquisaId);

        $pesquisas->titulo = $dados->titulo;
        $pesquisas->descricao = $dados->descricao;
        $pesquisas->pergunta = $dados->pergunta;
        $pesquisas->resposta = $dados->resposta;
        $pesquisas->save();

        header("HTTP/1.1 201 Created");
        echo json_encode(array("response" => "Pesquisa atualizada com sucesso!"));

    }

    static function DeletarPesquisa() {

        $pesquisaId = filter_input(INPUT_GET, "id_pesquisa");

        if(!$pesquisaId) {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response" => "Id da pesquisa não informado!"));
            exit;
        }

        $pesquisa = Pesquisa::destroy($pesquisaId);


        if(!$pesquisa) {
            header("HTTP/1.1 200 Ok");
            echo json_encode(array("response" => "Nenhuma pesquisa localizada!"));
            exit;
        }

        if($pesquisa) {
            header("HTTP/1.1 200 Ok");
            echo json_encode(array("response" => "Pesquisa removida com sucesso!"));
        } else {
            header("HTTP/1.1 200 Ok");
            echo json_encode(array("response" => "Não foi possivel remover nenhuma pesquisa!"));
        }

    }

}