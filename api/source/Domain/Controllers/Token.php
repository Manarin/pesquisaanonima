<?php
namespace Source\Controllers;

use DateTime;
use Firebase\JWT\JWT;

class CriarToken {
    static function token($usuarios){

        $key = "123465";

        $header = [
            'typ' => 'JWT',
            'alg' => 'HS265'
        ];

        $payload = [
            'exp' => (new DateTime("+2days"))->getTimestamp(),
            'sub' => $usuarios
            //'name' => $usuario
        ];

        $header = json_encode($header);
        $payload = json_encode($payload);


        $header = base64_encode($header);
        $payload = base64_encode($payload);

        $signature = hash_hmac('sha256', $header . "." . $payload, $key, true);
        $signature = base64_encode($signature);

        $token = $header . '.' . $payload . '.'. $signature;


        return $token;
    }

}