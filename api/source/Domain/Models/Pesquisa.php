<?php 

namespace Source\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Pesquisa extends Eloquent {

    public $timestamps = false;
    protected $table = 'pesquisa';
    protected $fillable = [

        'titulo', 'descricao', 'pergunta', 'resposta'

    ];

};